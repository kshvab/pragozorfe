# Pragozor Portal

This is Pragozor Portal, Angular&TypeScript SPA. It fetches content from Pragozor CMS and widgets data from Pragozor BE API.

Pragozor is a web application intended for the public, which can learn interesting numbers and analyzes about Prague.
It is part of the Golemio Data Dlatform, developed in Angular&TypeScript.

Developed by http://operatorict.cz

## Prerequisites

- Pragozor CMS (../cms/)
- Pragozor BE API or mock it (https://pragozorbe.docs.apiary.io/#reference/0/widgets-data/get-widgets-data)

## Installation & run

### Docker

Build docker image by `docker build -t portal .`

Setup config in `/portal/src/configs/{dev/prod}/config.json`. 
Run container by

```
docker run --rm \
    -p 4200:4200 \ # expose port 4200
    -e PORT=4200 \
    portal # docker image label (defined by step 1)
```

### Without docker

Install all dependencies using command:

```
yarn install
```

Then run 

```
ng {serve/build} --configuration={development/production}
```

More about development down below.

## Development

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact roszkowski.wojciech@operatorict.cz
