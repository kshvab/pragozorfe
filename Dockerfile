FROM node:16-alpine

ARG PROJECT_NAME_FOLDER=dist
WORKDIR /user/src/app/

RUN yarn global add serve

COPY --chown=node:node ./$PROJECT_NAME_FOLDER/ .

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]
