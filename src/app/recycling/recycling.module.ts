import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecyclingRoutingModule } from './recycling-routing.module';

import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.RecyclingComponent];

const SERVICES = [services.RecyclingService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, RecyclingRoutingModule],
})
export class RecyclingModule {}
