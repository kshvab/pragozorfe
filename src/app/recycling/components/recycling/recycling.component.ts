import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { filter, map, delay, takeUntil, tap } from 'rxjs/operators';
import { FullWidthBannerComponent, BaseComponent } from '@shared/components';
import { CmsPageModel } from '@shared/models/cms';
import { CmsService, CrawlerHelperService, ScrollService, StateService } from '@core/services';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'pr-recycling',
  templateUrl: './recycling.component.html',
  styleUrls: ['./recycling.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecyclingComponent extends BaseComponent implements OnInit {
  @ViewChild('banner') banner: FullWidthBannerComponent;
  cmsData$: Observable<CmsPageModel>;
  showErrorMsg$: Observable<boolean>;
  activeUrl: string;
  scrolledToSection = null;

  constructor(
    cmsService: CmsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    scrollService: ScrollService,
    state: StateService,
    private meta: Meta,
    private crawlerHelper: CrawlerHelperService
  ) {
    super();
    this.cmsData$ = cmsService.getCmsDataForPage('recyclation');
    this.activeUrl = router.url;
    const sectionToScroll$: Observable<string> = combineLatest([
      activatedRoute.queryParams,
      this.cmsData$,
    ]).pipe(
      delay(1000),
      tap(([queryParam, _]) => {
        if (queryParam.section == null) {
          scrollService.scrollToSection('recycling-heading', -30);
          state.setLoadingVisibility(false);
        } else {
          this.crawlerHelper.updateMetaForCrawlers(_.sections, queryParam.section, this.meta);
        }
      }),
      filter(([queryParam, cmsData]) => {
        return queryParam.section != null && cmsData != null;
      }),
      map(([queryParam, cmsData]) =>
        cmsData.widgetIds.indexOf(queryParam.section) > -1 ||
        cmsData.sectionUrls.indexOf(queryParam.section) > -1
          ? (queryParam.section as string)
          : null
      )
    );
    this.showErrorMsg$ = sectionToScroll$.pipe(
      map((section) => {
        return section == null;
      })
    );
    sectionToScroll$
      .pipe(
        delay(2000),
        tap((section) => {
          if (section != null) {
            scrollService.scrollToSectionWithoutAnimation(section, -25);
          } else {
            scrollService.scrollToSectionWithoutAnimation('unavailable-widget');
          }
          state.setLoadingVisibility(false);
        }),
        takeUntil(this.baseSubject)
      )
      .subscribe();
  }

  ngOnInit() {}
}
