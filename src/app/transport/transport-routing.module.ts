import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransportComponent } from './components/transport/transport.component';

const routes: Routes = [{ path: '', component: TransportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportRoutingModule {}
