export interface StateModel {
  isContactVisible: boolean;
  isMissingSectionVisible: boolean;
  loadingVisible: boolean;
}
