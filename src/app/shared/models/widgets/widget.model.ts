import { MetadataModel } from './metadata.model';

export interface WidgetModel<T> {
  metadata: MetadataModel;
  data: T;
}
