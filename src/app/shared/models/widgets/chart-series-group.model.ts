import { ChartNameValueModel } from './chart-name-value.model';

export interface ChartSeriesGroupModel {
  name: string;
  series: ChartNameValueModel[];
}
