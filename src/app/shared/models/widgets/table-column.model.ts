export interface TableColumnModel {
  value: string | number;
  highlighted: boolean;
}
