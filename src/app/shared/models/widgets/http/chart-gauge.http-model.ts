import { ChartMetadataHttpModel, WidgetHttpModel } from './common';

export interface ChartGaugeHttpModel
  extends WidgetHttpModel<
    ChartMetadataHttpModel,
    {
      value: number;
      min: number;
      max: number;
      unit: string;
      unit_en: string;
      label: string;
      label_en: string;
    }
  > {}
