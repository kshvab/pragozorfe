import { WidgetHttpModel, BaseMetadataHttpModel } from './common';

export interface MapPraguePoiHttpModel
  extends WidgetHttpModel<
    BaseMetadataHttpModel,
    {
      title: string;
      label_color: string;
      lat: number;
      lng: number;
      lon: number;
      description: string;
    }[]
  > {}
