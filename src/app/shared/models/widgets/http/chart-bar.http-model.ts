import { ChartMetadataHttpModel, WidgetHttpModel } from './common';

export interface ChartBarHttpModel
  extends WidgetHttpModel<ChartMetadataHttpModel, any[]> {}
