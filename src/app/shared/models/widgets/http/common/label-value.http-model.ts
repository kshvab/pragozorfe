export interface LabelValueHttpModel {
  label: string;
  label_en: string;
  value: number;
  unit?: string;
  unit_en: string;
}
