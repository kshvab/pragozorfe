import { BaseMetadataHttpModel } from './base-metadata.http-model';
import { LabelNameHttpModel } from './label-name.http-model';

export interface TableMetadataHttpModel extends BaseMetadataHttpModel {
  header_columns: LabelNameHttpModel[];
}
