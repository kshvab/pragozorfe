export type MetadataType =
  | 'chart-bar-vertical'
  | 'chart-bar-horizontal'
  | 'chart-bar-stack'
  | 'chart-line'
  | 'chart-pie'
  | 'chart-donut'
  | 'table'
  | 'visual-number'
  | 'gauge'
  | 'map-prague-pois'
  | 'map-prague-districts';
