import { MetadataType } from './metadata.type';

export interface BaseMetadataHttpModel {
  visual_type: MetadataType;
  updated_at: string;
  refresh_interval: number;
  source: string;
  source_url: string;
  unit: string;
  unit_en: string;
}
