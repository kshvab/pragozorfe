import { LabelNameHttpModel } from './label-name.http-model';

export interface AxisXHttpModel {
  name: string;
  label: string;
  series: LabelNameHttpModel[];
}
