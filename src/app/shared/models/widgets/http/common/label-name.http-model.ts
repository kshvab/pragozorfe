export interface LabelNameHttpModel {
  label: string;
  name: string;
}
