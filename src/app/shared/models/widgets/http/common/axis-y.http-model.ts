import { LabelNameHttpModel } from './label-name.http-model';

export interface AxisYHttpModel {
  label: string;
  series: LabelNameHttpModel[];
}
