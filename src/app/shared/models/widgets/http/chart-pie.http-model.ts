import {
  ChartMetadataHttpModel,
  LabelValueHttpModel,
  WidgetHttpModel,
} from './common';

export interface ChartPieHttpModel
  extends WidgetHttpModel<ChartMetadataHttpModel, LabelValueHttpModel[]> {}
