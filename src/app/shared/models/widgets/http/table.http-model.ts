import { TableMetadataHttpModel, WidgetHttpModel } from './common';

export interface TableHttpModel
  extends WidgetHttpModel<TableMetadataHttpModel, any[]> {}
