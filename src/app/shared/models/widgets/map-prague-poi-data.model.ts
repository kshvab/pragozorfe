export interface MapPraguePoiDataModel {
  title: string;
  labelColor: string;
  lat: number;
  lng: number;
  lon: number;
  description: string;
}
