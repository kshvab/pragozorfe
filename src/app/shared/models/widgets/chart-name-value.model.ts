export interface ChartNameValueModel {
  name: string;
  value: string | number;
}
