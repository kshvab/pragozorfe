import { CmsLocalizedTextModel } from '../cms';

export interface MapPragueDistrictDataModel {
  districtId: string;
  title: string;
  value: number;
  unit: CmsLocalizedTextModel;
}
