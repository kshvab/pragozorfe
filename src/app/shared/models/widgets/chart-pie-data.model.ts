import { ChartNameValueModel } from './chart-name-value.model';

export interface ChartPieDataModel {
  data: ChartNameValueModel[];
  unit: string;
}
