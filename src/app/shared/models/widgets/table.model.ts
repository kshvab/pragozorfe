import { TableColumnModel } from './table-column.model';

export interface TableDataModel {
  headers: string[];
  table: TableColumnModel[][];
}
