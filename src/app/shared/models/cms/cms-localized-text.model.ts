import { CmsLocalizedTextHttpModel } from './http';

// tslint:disable-next-line: no-empty-interface
export interface CmsLocalizedTextModel extends CmsLocalizedTextHttpModel {}
