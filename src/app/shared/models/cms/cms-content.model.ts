import { CmsContentType } from './http/cms-content.type';
import { WidgetModel } from '../widgets/widget.model';
import { CmsTextModel } from './cms-text.model';
import { CmsWidgetSettingsModel } from './cms-widget-settings.model';
import { CmsCrawlerDataModel } from './cms-crawler-data.model';
import { CmsPrahaPracujeWidgetHttpModel } from './http/cms-prahapracujewidget.http-model';

export interface CmsContentModel {
  image?: string;
  prahaPracujeWidget?: CmsPrahaPracujeWidgetHttpModel;
  text?: CmsTextModel;
  widget?: CmsWidgetSettingsModel;
  widgetData?: WidgetModel<any>;
  crawlerData?: CmsCrawlerDataModel;
  contentType: CmsContentType;
}
