import { CmsSectionModel } from './cms-section.model';

export interface CmsPageModel {
  widgetIds: string[];
  sectionUrls: string[];
  sections: CmsSectionModel[];
}
