import { CmsLocalizedTextHttpModel } from './http';
import { CmsRowModel } from './cms-row.model';

export interface CmsSectionModel {
  sectionUrl: string;
  sectionName: CmsLocalizedTextHttpModel;
  showHeader: boolean;
  rows: CmsRowModel[];
  order: number;
}
