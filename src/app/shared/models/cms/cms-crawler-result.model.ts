import { CmsLocalizedTextModel } from './cms-localized-text.model';

export interface CmsCrawlerResultModel {
  title: CmsLocalizedTextModel;
  description: CmsLocalizedTextModel;
}
