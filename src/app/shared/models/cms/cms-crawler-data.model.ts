import { CmsLocalizedTextModel } from './cms-localized-text.model';

export interface CmsCrawlerDataModel {
  id_ref: string;
  header: CmsLocalizedTextModel;
  paragraph: CmsLocalizedTextModel;
}
