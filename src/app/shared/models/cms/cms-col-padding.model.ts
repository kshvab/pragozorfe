export interface CmsColPaddingModel {
  top: number;
  left: number;
  right: number;
  bottom: number;
}
