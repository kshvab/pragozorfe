import { CmsLocalizedTextModel } from './cms-localized-text.model';
import { CmsTextType } from './http/cms-text.type';
import { CmsTextAlignmentType } from './http/cms-text-alignment.type';

export interface CmsTextModel {
  text: CmsLocalizedTextModel;
  type: CmsTextType;
  alignment: CmsTextAlignmentType[];
}
