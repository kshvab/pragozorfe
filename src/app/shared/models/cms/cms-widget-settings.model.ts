import { CmsMapIconSizeType } from './http/cms-map-icon-size.type';
import { CmsLegendPositionType } from './http/cms-legend-position.type';

export interface CmsWidgetSettingsModel {
  id: string;
  colorScheme?: { domain: string[] };
  schemeType?: 'ordinal' | 'linear';
  gradient?: boolean;
  showDataLabel?: boolean;
  showXAxisLabel?: boolean;
  showYAxisLabel?: boolean;
  showLegend?: boolean;
  chartHeight?: number;
  showLabels?: boolean;
  marginBottom?: number;
  gaugeShowAxis?: boolean;
  legendMinHeight?: number;
  chartTitle?: string;
  hideTooltips?: boolean;
  tooltipDisabled?: boolean;
  mapIconSize?: CmsMapIconSizeType;
  legendPosition?: CmsLegendPositionType;
  roundEdges?: boolean;
  animations?: boolean;
  xAxisSingleTicks?: boolean;
}
