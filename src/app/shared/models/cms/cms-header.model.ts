import { CmsLocalizedTextModel } from './cms-localized-text.model';
import { CmsHeaderSliderItemModel } from './cms-header-slider-item.model';

export interface CmsHeaderModel {
  title: CmsLocalizedTextModel;
  subtitle: CmsLocalizedTextModel;
  sliders: CmsHeaderSliderItemModel[];
}
