export type CmsPageType =
  | 'mobility'
  | 'transport'
  | 'natural-environment'
  | 'tourism'
  | 'health'
  | 'housing'
  | 'cars'
  | 'mobility'
  | 'recycling'
  | 'security'
  | 'citymanagement';
