import { CmsContentModel } from './cms-content.model';
import { CmsRowModel } from './cms-row.model';
import { CmsColPaddingModel } from './cms-col-padding.model';
import { CmsShareInfoModel } from './cms-share-info.model';

export interface CmsColModel {
  type: string;
  content?: CmsContentModel;
  isPanel?: boolean;
  rows?: CmsRowModel[];
  padding: number | CmsColPaddingModel;
  widgetIds: string[];
}
