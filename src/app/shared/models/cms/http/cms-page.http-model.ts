import { CmsSectionHttpModel } from './cms-section.http-model';

export interface CmsPageHttpModel {
  sections: CmsSectionHttpModel[];
}
