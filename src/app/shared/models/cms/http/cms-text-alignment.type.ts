export type CmsTextAlignmentType =
  | 'text-sm-left'
  | 'text-md-left'
  | 'text-lg-left'
  | 'text-xl-left'
  | 'text-sm-center'
  | 'text-md-center'
  | 'text-lg-center'
  | 'text-xl-center'
  | 'text-sm-right'
  | 'text-md-right'
  | 'text-lg-right'
  | 'text-xl-right';
