import { CmsLocalizedTextHttpModel } from './cms-localized-text.http-model';
import { CmsRowHttpModel } from './cms-row.http-model';

export interface CmsSectionHttpModel {
  section_url: string;
  section_name: CmsLocalizedTextHttpModel;
  show_header: boolean;
  rows: CmsRowHttpModel[];
  order: number;
}
