import { CmsLocalizedTextHttpModel } from './cms-localized-text.http-model';

export interface CmsCrawlerDataHttpModel {
  id_ref: string;
  header: CmsLocalizedTextHttpModel;
  paragraph: CmsLocalizedTextHttpModel;
}
