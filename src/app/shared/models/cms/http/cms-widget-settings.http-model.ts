import { CmsMapIconSizeType } from './cms-map-icon-size.type';
import { CmsLegendPositionType } from './cms-legend-position.type';

export interface CmsWidgetSettingsHttpModel {
  id: string;
  color_scheme?: { domain: string[] };
  scheme_type?: 'ordinal' | 'linear';
  gradient?: boolean;
  show_data_label?: boolean;
  show_data_label_when_touch?: boolean;
  show_x_axis_label?: boolean;
  show_y_axis_label?: boolean;
  show_legend?: boolean;
  labels: boolean;
  chart_height: number;
  margin_bottom?: number;
  gauge_show_axis?: boolean;
  legend_min_height?: number;
  chart_title?: string;
  tooltip_disabled?: boolean;
  hide_tooltips?: boolean;
  hide_tooltips_when_touch?: boolean;
  map_icon_size?: CmsMapIconSizeType;
  legend_position?: CmsLegendPositionType;
  round_edges?: boolean;
  animations?: boolean;
  x_axis_single_ticks: boolean;
}
