import { CmsLocalizedTextHttpModel } from './cms-localized-text.http-model';
import { CmsHeaderSliderItemHttpModel } from './cms-header-slider-item.http-model';

export interface CmsHeaderHttpModel {
  title: CmsLocalizedTextHttpModel;
  subtitle: CmsLocalizedTextHttpModel;
  sliders: CmsHeaderSliderItemHttpModel[];
}
