import { CmsContentHttpModel } from './cms-content.http-model';
import { CmsColPaddingHttpModel } from './cms-col-padding.http-model';
import { CmsRowHttpModel } from './cms-row.http-model';

export interface CmsColHttpModel {
  type: string;
  content?: CmsContentHttpModel;
  rows?: CmsRowHttpModel[];
  is_panel?: boolean;
  padding: number | CmsColPaddingHttpModel;
}
