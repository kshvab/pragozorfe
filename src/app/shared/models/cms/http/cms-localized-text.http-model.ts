export interface CmsLocalizedTextHttpModel {
  text: string;
  text_en: string;
}
