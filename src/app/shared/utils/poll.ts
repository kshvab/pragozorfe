import { Observable, timer } from 'rxjs';
import { concatMap, expand } from 'rxjs/operators';

export function poll<T>(fn: (res: T) => number) {
  return (source$: Observable<T>) =>
    source$.pipe(
      expand((req) => timer(fn(req)).pipe(concatMap(() => source$)))
    );
}
