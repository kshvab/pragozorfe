import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ElementRef,
  AfterViewInit,
  HostBinding,
  ViewChild,
  forwardRef,
  ChangeDetectorRef,
  Renderer2,
} from '@angular/core';
import { WidgetModel, ChartDataModel } from '@shared/models/widgets';
import { CmsWidgetSettingsModel } from '@shared/models';
import { BarVerticalComponent, ColorHelper } from '@swimlane/ngx-charts';
import { WidgetWithLegendComponent } from '../base';

@Component({
  selector: 'pr-chart-bar-vertical',
  templateUrl: './chart-bar-vertical.component.html',
  styleUrls: ['./chart-bar-vertical.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: WidgetWithLegendComponent,
      useExisting: forwardRef(() => ChartBarVerticalComponent),
    },
  ],
})
export class ChartBarVerticalComponent extends WidgetWithLegendComponent
  implements OnInit, AfterViewInit {
  @ViewChild(BarVerticalComponent) chart: BarVerticalComponent;
  @ViewChild('legend', { read: ElementRef, static: true })
  legendComponent: ElementRef;
  isDetailVisible: boolean;
  @Input() settings: CmsWidgetSettingsModel;
  @HostBinding('style.height.px')
  get height() {
    return this.settings?.chartHeight;
  }
  // tslint:disable-next-line:variable-name
  private _data: WidgetModel<ChartDataModel>;
  @Input() set data(data: WidgetModel<ChartDataModel>) {
    this._data = data;

    this.legendData = data.data.series.map((d) => d.name);
    this.legendColors = new ColorHelper(
      this.settings.colorScheme,
      'ordinal',
      this.legendData,
      null
    );
  }
  get data(): WidgetModel<ChartDataModel> {
    return this._data;
  }

  legendData;
  legendColors;

  view: any[] = undefined;

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  tooltipDisabled = false;
  showDataLabel = false;
  showXAxisLabel = false;
  showYAxisLabel = false;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };
  get legendHeight(): number {
    return (
      this.legendComponent?.nativeElement.offsetHeight ||
      this.settings.legendMinHeight
    );
  }

  constructor(
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2,) {
    super();
  }

  ngOnInit() {}

  ngAfterViewInit() {}

  onSelect(event) {}
  legendActivate(data) {
    this.chart.onActivate(data);
  }

  legendDeactivate(data) {
    this.chart.onDeactivate(data);
  }

  showChartInPopup(){
    this.isDetailVisible = true;
    this.cdr.markForCheck();
    this.renderer.addClass(document.body, 'hide-scroll');
  }

  closeChartDetail(){
    this.isDetailVisible = false;
    this.cdr.markForCheck();
    this.renderer.removeClass(document.body, 'hide-scroll');
  }
}
