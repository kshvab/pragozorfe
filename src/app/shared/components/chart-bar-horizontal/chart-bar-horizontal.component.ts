import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ElementRef,
  AfterViewInit,
  HostBinding,
  ViewChild,
  forwardRef,
} from '@angular/core';
import { WidgetModel, ChartDataModel } from '@shared/models/widgets';
import { CmsWidgetSettingsModel } from '@shared/models';
import {
  LegendComponent,
  BarHorizontalComponent,
  ColorHelper,
} from '@swimlane/ngx-charts';
import { WidgetWithLegendComponent } from '../base';
import { ChangeDetectorRef } from '@angular/core';
import { Renderer2 } from '@angular/core';

@Component({
  selector: 'pr-chart-bar-horizontal',
  templateUrl: './chart-bar-horizontal.component.html',
  styleUrls: ['./chart-bar-horizontal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: WidgetWithLegendComponent,
      useExisting: forwardRef(() => ChartBarHorizontalComponent),
    },
  ],
})
export class ChartBarHorizontalComponent extends WidgetWithLegendComponent
  implements OnInit, AfterViewInit {
  @ViewChild(BarHorizontalComponent) chart: BarHorizontalComponent;
  @ViewChild('legend', { read: ElementRef, static: true })
  legendComponent: ElementRef;
  @Input() settings: CmsWidgetSettingsModel;
  @HostBinding('style.height.px')
  get height() {
    return this.settings?.chartHeight;
  }
  isDetailVisible: boolean;
  // tslint:disable-next-line:variable-name
  private _data: WidgetModel<ChartDataModel>;
  @Input() set data(data: WidgetModel<ChartDataModel>) {
    this._data = data;

    this.legendData = data.data.series.map((d) => d.name);
    this.legendColors = new ColorHelper(
      this.settings.colorScheme,
      'ordinal',
      this.legendData,
      null
    );
  }
  get data(): WidgetModel<ChartDataModel> {
    return this._data;
  }

  legendData;
  legendColors;

  view: any[] = undefined;

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  tooltipDisabled = false;
  showDataLabel = false;
  showXAxisLabel = true;
  showYAxisLabel = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };

  constructor(
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2) {
    super();
  }
  get legendHeight(): number {
    return (
      this.legendComponent?.nativeElement.offsetHeight ||
      this.settings.legendMinHeight
    );
  }

  ngOnInit() {}

  ngAfterViewInit() {}
  onSelect(data): void {}

  onActivate(data): void {}

  onDeactivate(data): void {}
  legendActivate(data) {
    this.chart.onActivate(data);
  }

  legendDeactivate(data) {
    this.chart.onDeactivate(data);
  }

  showChartInPopup(){
    this.isDetailVisible = true;
    this.cdr.markForCheck();
    this.renderer.addClass(document.body, 'hide-scroll');
  }

  closeChartDetail(){
    this.isDetailVisible = false;
    this.cdr.markForCheck();
    this.renderer.removeClass(document.body, 'hide-scroll');
  }
}
