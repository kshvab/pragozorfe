import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CitymanagementComponent } from './components/citymanagement/citymanagement.component';

const routes: Routes = [{ path: '', component: CitymanagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CitymanagementRoutingModule {}
