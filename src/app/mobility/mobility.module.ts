import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MobilityRoutingModule } from './mobility-routing.module';

import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.MobilityComponent];

const SERVICES = [services.MobilityService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, MobilityRoutingModule],
})
export class MobilityModule {}
