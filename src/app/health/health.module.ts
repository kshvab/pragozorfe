import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HealthRoutingModule } from './health-routing.module';
import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.HealthComponent];

const SERVICES = [services.HealthService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, HealthRoutingModule],
})
export class HealthModule {}
