import { Router, ActivatedRoute } from '@angular/router';
import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
} from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { filter, map, takeUntil, delay, tap } from 'rxjs/operators';
import { CmsService, CrawlerHelperService, ScrollService, StateService } from '@core/services';
import { CmsPageModel } from '@shared/models/cms';
import { FullWidthBannerComponent, BaseComponent } from '@shared/components';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'pr-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecurityComponent extends BaseComponent implements OnInit {
  @ViewChild('banner') banner: FullWidthBannerComponent;
  cmsData$: Observable<CmsPageModel>;
  showErrorMsg$: Observable<boolean>;
  activeUrl: string;
  scrolledToSection = null;

  constructor(
    cmsService: CmsService,
    router: Router,
    activatedRoute: ActivatedRoute,
    scrollService: ScrollService,
    state: StateService,
    private meta: Meta,
    private crawlerHelper: CrawlerHelperService
  ) {
    super();
    this.cmsData$ = cmsService.getCmsDataForPage('security');
    this.activeUrl = router.url;

    const sectionToScroll$: Observable<string> = combineLatest([
      activatedRoute.queryParams,
      this.cmsData$,
    ]).pipe(
      delay(1000),
      tap(([queryParam, _]) => {
        if (queryParam.section == null) {
          scrollService.scrollToSection('security-heading', -30);
          state.setLoadingVisibility(false);
        } else {
          this.crawlerHelper.updateMetaForCrawlers(_.sections, queryParam.section, this.meta);
        }
      }),
      filter(([queryParam, cmsData]) => {
        return queryParam.section != null && cmsData != null;
      }),
      map(([queryParam, cmsData]) =>
        cmsData.widgetIds.indexOf(queryParam.section) > -1 ||
        cmsData.sectionUrls.indexOf(queryParam.section) > -1
          ? (queryParam.section as string)
          : null
      )
    );
    this.showErrorMsg$ = sectionToScroll$.pipe(
      map((section) => {
        return section == null;
      })
    );
    sectionToScroll$
      .pipe(
        delay(2000),
        tap((section) => {
          if (section != null) {
            scrollService.scrollToSectionWithoutAnimation(section, -25);
          } else {
            scrollService.scrollToSectionWithoutAnimation('unavailable-widget');
          }
          state.setLoadingVisibility(false);
        }),
        takeUntil(this.baseSubject)
      )
      .subscribe();
  }

  ngOnInit() {}
}
