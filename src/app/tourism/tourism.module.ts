import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TourismRoutingModule } from './tourism-routing.module';
import * as components from './components';
import * as services from './services';
import { SharedModule } from '@shared/shared.module';

const COMPONENTS = [components.TourismComponent];

const SERVICES = [services.TourismService];

@NgModule({
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
  imports: [SharedModule, CommonModule, TourismRoutingModule],
})
export class TourismModule {}
