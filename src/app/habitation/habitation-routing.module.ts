import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HabitationComponent } from './components/habitation/habitation.component';

const routes: Routes = [{ path: '', component: HabitationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HabitationRoutingModule {}
