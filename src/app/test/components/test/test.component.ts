import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { MapPragueDistrictDataModel } from '@shared/models';

@Component({
  selector: 'pr-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestComponent extends BaseComponent implements OnInit {
  updatedAt = new Date();
  districts: MapPragueDistrictDataModel[] = [];

  constructor() {
    super();
    for (let i = 1; i <= 22; i++) {
      this.districts.push({
        districtId: 'prague-' + i,
        title: 'Praha ' + i,
        value: parseInt(i + '000', 10),
        unit: {
          text: 'Kč/m2',
          text_en: 'CZK/m2',
        },
      });
    }
  }

  ngOnInit() {}
}
