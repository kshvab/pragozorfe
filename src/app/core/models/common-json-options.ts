import { CommonOptions } from './common-options';

export interface CommonJsonOptions extends CommonOptions {
  observe: 'events';
  responseType?: 'json';
}
