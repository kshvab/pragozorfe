export interface EnvelopeError {
  key: string;
  code: string;
  description: string;
}
