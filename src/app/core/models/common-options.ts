import { HttpHeaders, HttpParams } from '@angular/common/http';

export interface CommonOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe: 'events' | 'body';
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json' | 'arraybuffer' | 'blob' | 'text';
    withCredentials?: boolean;
}
