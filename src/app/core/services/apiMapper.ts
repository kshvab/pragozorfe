import { CmsPageHttpModel } from '@shared/models';

export function apiMaper(data: any): CmsPageHttpModel {
  console.log(data);

  let pageDescriptionHeader = {
    section_name: {
      text: data.title,
      text_en: data.title,
    },
    section_name_en: null,
    rows: [
      {
        cols: [
          {
            rows: [
              {
                cols: [
                  {
                    rows: [
                      {
                        cols: [
                          {
                            type: 'col-md-12',
                            content: {
                              text: {
                                text: {
                                  text: data.subTitle,
                                  text_en: data.subTitle,
                                },
                                type: 'h3',
                                alignment: ['text-md-left', 'text-center'],
                              },
                              content_type: 'text',
                            },
                          },
                          {
                            type: 'col-md-12 fontsize-lg my-3',
                            content: {
                              text: {
                                text: {
                                  text: data.description,
                                  text_en: data.description,
                                },
                                type: 'paragraph',
                                alignment: ['text-md-left', 'text-center'],
                              },
                              content_type: 'text',
                            },
                          },
                        ],
                      },
                    ],
                    type: 'col-md-5',
                  },
                  {
                    type: 'col-md-7',
                    content: {
                      image: data.image.data.attributes.url,
                      content_type: 'image',
                    },
                  },
                ],
              },
            ],
            type: 'col-md-12',
            is_panel: false,
          },
        ],
      },
    ],
    section_url: data.slug,
    //order: 1,
    show_header: null,
  };

  const cardsRows = (chapterCardsArr) => {
    let cardsArr = [];

    chapterCardsArr.forEach((card) => {
      cardsArr.push({
        cols: [
          {
            rows: [
              {
                cols: [
                  {
                    rows: [
                      {
                        cols: [
                          {
                            type: 'col-md-12',
                            content: {
                              text: {
                                text: {
                                  text: card.title,
                                  text_en: '______',
                                },
                                type: 'h4',
                              },
                              content_type: 'text',
                            },
                          },
                          {
                            type: 'col-md-12',
                            content: {
                              text: {
                                text: {
                                  text: card.description,
                                  text_en: '______',
                                },
                                type: 'paragraph',
                              },
                              content_type: 'text',
                            },
                          },
                          {
                            type: 'col-md-12',
                            content: {
                              content_type: 'share-info',
                            },
                          },
                        ],
                      },
                    ],
                    type: `col-md-${12 - card.widgetWidth}`,
                  },
                  {
                    type: `col-md-${card.widgetWidth}`,
                    content: {
                      widget: card.staticDatasource,
                      content_type: 'widget',
                    },
                  },
                ],
              },
            ],
            type: 'col-md-12',
            is_panel: true,
          },
        ],
      });
    });

    return cardsArr;
  };

  const chapterSections = (chaptersArr) => {
    let resultArr = [];
    chaptersArr.forEach((chapter) => {
      resultArr.push({
        id: 45,
        section_name: {
          text: chapter.title,
          text_en: chapter.title,
        },
        section_name_en: null,
        rows: cardsRows(chapter.cards),
        section_url: 'health-covid',
        order: 0,
        show_header: true,
      });
    });

    return resultArr;
  };

  let result = {
    sections: [pageDescriptionHeader, ...chapterSections(data.chapters)],
  };

  return result;
}
