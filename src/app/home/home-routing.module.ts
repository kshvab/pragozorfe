import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent, HomeWelcomeComponent } from './components';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: HomeWelcomeComponent,
      },
      {
        path: 'natural-environment',
        loadChildren: () =>
          import('../natural-environment/natural-environment.module').then(
            (m) => m.NaturalEnvironmentModule
          ),
      },
      {
        path: 'transport',
        loadChildren: () =>
          import('../transport/transport.module').then(
            (m) => m.TransportModule
          ),
      },
      {
        path: 'tourism',
        loadChildren: () =>
          import('../tourism/tourism.module').then((m) => m.TourismModule),
      },
      {
        path: 'housing',
        loadChildren: () =>
          import('../habitation/habitation.module').then(
            (m) => m.HabitationModule
          ),
      },
      {
        path: 'cars',
        loadChildren: () =>
          import('../cars/cars.module').then((m) => m.CarsModule),
      },
      {
        path: 'health',
        loadChildren: () =>
          import('../health/health.module').then((m) => m.HealthModule),
      },
      {
        path: 'mobility',
        loadChildren: () =>
          import('../mobility/mobility.module').then((m) => m.MobilityModule),
      },
      {
        path: 'recycling',
        loadChildren: () =>
          import('../recycling/recycling.module').then(
            (m) => m.RecyclingModule
          ),
      },
      {
        path: 'security',
        loadChildren: () =>
          import('../security/security.module').then((m) => m.SecurityModule),
      },
      {
        path: 'citymanagement',
        loadChildren: () =>
          import('../citymanagement/citymanagement.module').then(
            (m) => m.CitymanagementModule
          ),
      },
      {
        path: 'test',
        loadChildren: () =>
          import('../test/test.module').then((m) => m.TestModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
