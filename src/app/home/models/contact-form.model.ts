export interface ContactFormModel {
  email: string;
  message: string;
  recaptcha: string;
}
