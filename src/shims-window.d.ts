export {};

declare global {
  interface Window {
    gtag: (type: string, code: string, options: Record<string, any>) => void;
    // Note: vanilla-cookieconsent does not provide types
    initCookieConsent: () => Record<string, any>;
    CookieConsent: Record<string, any>;
  }
}
